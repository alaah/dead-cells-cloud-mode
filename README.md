![screenshot](https://steamuserimages-a.akamaihd.net/ugc/945077538414275479/845D69583700A7FA44BDCB8515C598A6375999BF/)

# Howto
This is suited for [barebones](https://gitlab.com/alaah/dead-cells-barebones).

# License
See LICENSE for information.

Of course this is based on absolutely proprietary files. I consider my "changes" to be GPL. No one will care anyway.